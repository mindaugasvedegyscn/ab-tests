<?php

declare(strict_types=1);

namespace AbTests\Client;

use AbTests\Model\Test;
use Doctrine\Common\Collections\ArrayCollection;

interface StorageClientInterface
{
    /**
     * @param ArrayCollection<int, Test> $tests
     */
    public function saveTests(ArrayCollection $tests): void;
}
