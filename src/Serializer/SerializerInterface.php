<?php

declare(strict_types=1);

namespace AbTests\Serializer;

use AbTests\Model\Test;
use Doctrine\Common\Collections\ArrayCollection;

interface SerializerInterface
{
    /**
     * @param ArrayCollection<int, Test> $tests
     */
    public function serialize(ArrayCollection $tests): string;
}
