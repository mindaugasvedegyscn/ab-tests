<?php

declare(strict_types=1);

namespace AbTests\Serializer;

use AbTests\Model\Test;
use Doctrine\Common\Collections\ArrayCollection;

class Serializer implements SerializerInterface
{
    public function serialize(ArrayCollection $tests): string
    {
        // TODO use Symfony Serializer.

        return json_encode(
            $tests
                ->map(
                    function (Test $test): array {
                        return [
                            'id' => $test->getId(),
                            'status' => $test->getStatus(),
                            'paths_include' => $test->getPathsInclude(),
                            'paths_exclude' => $test->getPathsExclude(),
                            'variant_chosen' => $test->getVariantChosen(),
                            'target_languages' => $test->getTargetLanguages(),
                            'variant_path' => $test->getVariantPath(),
                        ];
                    }
                )
                ->toArray(),
            \JSON_THROW_ON_ERROR,
        );
    }
}
