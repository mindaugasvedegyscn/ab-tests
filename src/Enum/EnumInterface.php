<?php

declare(strict_types=1);

namespace AbTests\Enum;

interface EnumInterface
{
    /**
     * @param mixed $enum
     */
    public static function isValid($enum): bool;

    /**
     * @param mixed $enum
     */
    public static function getKey($enum): string;

    /**
     * @return array<mixed, mixed>
     */
    public static function getValues(): array;
}
