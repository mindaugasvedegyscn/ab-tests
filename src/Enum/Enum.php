<?php

declare(strict_types=1);

namespace AbTests\Enum;

abstract class Enum implements EnumInterface
{
    public static function isValid($enum): bool
    {
        return \in_array($enum, static::getValues(), true);
    }

    public static function getKey($enum): string
    {
        return array_search($enum, static::getValues(), true);
    }

    /**
     * @return array<mixed, mixed>
     */
    abstract public static function getValues(): array;
}
