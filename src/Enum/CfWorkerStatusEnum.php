<?php

namespace AbTests\Enum;

/**
 * Enum of Test statuses available in CF Workers.
 */
class CfWorkerStatusEnum extends Enum
{
    public const ACTIVE = '1';
    public const STOPPED = '0';

    /**
     * @return string[]
     */
    public static function getValues(): array
    {
        return [
            self::ACTIVE,
            self::STOPPED,
        ];
    }
}
