<?php

declare(strict_types=1);

namespace AbTests\Model;

use AbTests\Enum\CfWorkerStatusEnum;

class Test
{
    private string $id;
    private string $status;

    /**
     * @var string[]
     */
    private array $pathsInclude;

    /**
     * @var string[]|null
     */
    private ?array $pathsExclude;

    /**
     * @var string[]|null
     */
    private ?array $targetLanguages;
    private ?string $variantPath;
    private ?string $variantChosen;

    /**
     * @param string[]      $pathsInclude
     * @param string[]|null $pathsExclude
     */
    public function __construct(
        string $id,
        string $status,
        array $pathsInclude,
        ?array $pathsExclude,
        ?string $variantChosen,
    ) {
        if (CfWorkerStatusEnum::isValid($status) === false) {
            throw new \InvalidArgumentException("Invalid status '{$status}'.");
        }

        $this->id = $id;
        $this->status = $status;
        $this->pathsInclude = $pathsInclude;
        $this->pathsExclude = $pathsExclude;
        $this->targetLanguages = null;
        $this->variantPath = null;
        $this->variantChosen = $variantChosen;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string[]
     */
    public function getPathsInclude(): array
    {
        return $this->pathsInclude;
    }

    /**
     * @return string[]|null
     */
    public function getPathsExclude(): ?array
    {
        return $this->pathsExclude;
    }

    /**
     * @return string[]|null
     */
    public function getTargetLanguages(): ?array
    {
        return $this->targetLanguages;
    }

    public function getVariantPath(): ?string
    {
        return $this->variantPath;
    }

    public function getVariantChosen(): ?string
    {
        return $this->variantChosen;
    }
}
