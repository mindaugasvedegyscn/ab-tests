<?php

declare(strict_types=1);

namespace AbTests\Tests\Unit\Serializer;

use AbTests\Enum\CfWorkerStatusEnum;
use AbTests\Model\Test;
use AbTests\Serializer\Serializer;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class SerializerTest extends TestCase
{
    private Serializer $serializer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->serializer = new Serializer();
    }

    public function testSerialize(): void
    {
        $test1 = new Test(
            id: 'lK7C4FvcQCaPWJ-12345-1',
            status: CfWorkerStatusEnum::ACTIVE,
            pathsInclude: ['/path/a/', '/a/b/'],
            pathsExclude: ['/paths/exclude/'],
            variantChosen: '1',
        );

        $test2 = new Test(
            id: 'lK7C4FvcQCaPWJ-12345-2',
            status: CfWorkerStatusEnum::STOPPED,
            pathsInclude: ['/path/a/', '/x/y/'],
            pathsExclude: ['/paths/exclude/'],
            variantChosen: '1',
        );

        $test3 = new Test(
            id: 'lK7C4FvcQCaPWJ-12345-3',
            status: CfWorkerStatusEnum::ACTIVE,
            pathsInclude: ['/path/a/', '/x/y/z/'],
            pathsExclude: null,
            variantChosen: null,
        );

        $test4 = new Test(
            id: 'lK7C4FvcQCaPWJ-12345-4',
            status: CfWorkerStatusEnum::STOPPED,
            pathsInclude: ['/path/a/', '/a/b/'],
            pathsExclude: null,
            variantChosen: '0',
        );

        $tests = new ArrayCollection([
            $test1,
            $test2,
            $test3,
            $test4,
        ]);

        $this->assertJsonStringEqualsJsonString(
            '
                [
                  {
                    "id": "lK7C4FvcQCaPWJ-12345-1",
                    "status": "1",
                    "paths_include": ["/path/a/", "/a/b/"],
                    "paths_exclude": ["/paths/exclude/"],
                    "variant_chosen": "1",
                    "target_languages": null,
                    "variant_path": null
                  },
                  {
                    "id": "lK7C4FvcQCaPWJ-12345-2",
                    "status": "0",
                    "paths_include": ["/path/a/", "/x/y/"],
                    "paths_exclude": ["/paths/exclude/"],
                    "variant_chosen": "1",
                    "target_languages": null,
                    "variant_path": null
                  },
                  {
                    "id": "lK7C4FvcQCaPWJ-12345-3",
                    "status": "1",
                    "paths_include": ["/path/a/", "/x/y/z/"],
                    "paths_exclude": null,
                    "variant_chosen": null,
                    "target_languages": null,
                    "variant_path": null
                  },
                  {
                    "id": "lK7C4FvcQCaPWJ-12345-4",
                    "status": "0",
                    "paths_include": ["/path/a/", "/a/b/"],
                    "paths_exclude": null,
                    "variant_chosen": "0",
                    "target_languages": null,
                    "variant_path": null
                  }
                ]
            ',
            $this->serializer->serialize($tests),
        );
    }
}
