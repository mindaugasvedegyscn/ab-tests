<?php

use PhpCsFixer\Finder;
use PhpQaPhpCs\Config;

$finder = (new Finder())
    ->in(['src', 'tests'])
;

return (new Config())
    ->setFinder($finder)
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@Symfony' => false,
            '@Symfony:risky' => false,
        ]
    )
    ->setCacheFile(__DIR__.'/.php_cs.cache');
