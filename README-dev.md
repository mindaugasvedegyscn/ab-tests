#### Tests

``
docker-compose run --rm php vendor/bin/phpunit
``

#### Code style

``
docker-compose run --rm php vendor/bin/php-cs-fixer fix
``

``
docker-compose run --rm php vendor/bin/phpstan analyse -c phpstan.neon --memory-limit=1G
``
